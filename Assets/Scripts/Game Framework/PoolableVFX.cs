﻿using Assets.Scripts.Optimalization;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(ParticleSystem))]
[RequireComponent(typeof(PoolableObject))]
public class PoolableVFX : MonoBehaviour {
    public void OnParticleSystemStopped()
    {
        ObjectPool.PushIntoPool(GetComponent<PoolableObject>());
    }
}
