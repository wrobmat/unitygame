﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class SfxPlayer : MonoBehaviour {

    [SerializeField]
    private List<EventAudioClipPair> m_Pairs = new List<EventAudioClipPair>();

    AudioSource m_Speaker;
    AudioSource Speaker
    {
        get
        {
            if(m_Speaker == null)
            {
                m_Speaker = GetComponent<AudioSource>();
            }

            return m_Speaker;
        }
    }

    void Awake()
    {
        foreach (var pair in m_Pairs)
        {
            Action playSoundAction = () =>
            {
                Speaker.PlayOneShot(pair.Clip);
            };

            pair.Event.Event += playSoundAction;
            pair.EventCache += playSoundAction;
        }
    }

    void OnDestroy()
    {
        foreach (var pair in m_Pairs)
        {
            pair.Event.Event -= pair.EventCache;
        }
    }
}

[System.Serializable]
class EventAudioClipPair
{
    public BasicGameplayEvent Event;
    public AudioClip Clip;
    public Action EventCache;
}
