﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Shared Int", menuName = "Shared Values/Int")]
public class SharedInt : ScriptableObject {
    public Action<int> OnChanged = delegate (int val) { };

    int m_Value = 0;
    public int Value
    {
        get
        {
            return m_Value;
        }
        set
        {
            if (value != m_Value)
            {
                m_Value = value;
                OnChanged.Invoke(value);
            }
        }
    }

}
