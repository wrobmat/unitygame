﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.UI;

public class VolumeController : MonoBehaviour {

    public AudioMixer Mixer;
    public string ParameterName;
    public Slider slider;

    void OnEnable()
    {
        float dbVolume = 0.0f;
        Mixer.GetFloat(ParameterName, out dbVolume);
        slider.value = GetLinear(dbVolume);
        slider.onValueChanged.AddListener(SetVolume);
    }

    void OnDisable()
    {
        slider.onValueChanged.RemoveListener(SetVolume);
    }

    public void SetVolume(float volume)
    {
        Mixer.SetFloat(ParameterName, GetDB(volume));
    }

    /*
     * Potrzebujemy rzutowac wartosci 0 do 1 na -80 do 0, jednocześnie emulując skalę logarytmową.
     * -5db to o 90%
     * -10db to ok 50%
     * -20db to ok 25%
     * -30db to ok 10%
     * -60db to ok 0.5%
     * -80db to 0%
     * 
     * Nie musimy być tutaj super prezycyjni, ale musimy mieć funkcje rzutującą z 0..1 na -80..0 - funkcjaA
     * oraz funkcje w drugą stronę - funkcjaB
     * i musi być zachowana własność x - funkcjaB(funkcjaA(x)) dla każdego x z zakresu 0..1
     */


    /*
     * Możemy zmienić założenie funkcjiA aby nie rzutowała na -80..0, tylko na -1..0 a potem tą wartość pomnożyć *80
     * 1.0 -> 0.0
     * 0.9 -> 0.0625
     * 0.5 -> -0.125
     * 0.25 -> -0.25 (to może być dobry punkt zaczepienia do szukania funkcji)
     * 0.1 -> -0.375
     * 0.05 -> -0.75
     * 0.0 -> -1
     */

    /*
     * Najłatwiej będzie nam zaproksymować to potęgami
     * 
     * w tym celu możemy założyć, że od wyniku potęgi (0..1)
     * odejmujemy 1, czyli z potęgi musimy dostać:
     * 1.0 -> 1
     * 0.9 -> 0.9475 (pow 1/4 daje 0.974)
     * 0.5 -> 0.875 (pow 1/4 daje 0.84)
     * 0.25 -> 0.75 (pow 1/4 daje 0.7)
     * 0.1 -> 0.625 (pow 1/4 daje 0.562)
     * 0.05 -> 0.25 (pow 1/4 daje 0.473 czyli tutaj się trochę rozjeżdża)
     * 0.0 -> 0
     */

    /*
     * czyli pow(1/4) daje nam dobrą aproksymację
     * z liniowego do DB, a pow(4)  później odwraca to na liinowe
     */

    float GetDB(float linear)
    {
        return Mathf.Lerp(-80.0f, 0.0f, Mathf.Pow(linear, 0.25f));
    }

    float GetLinear(float db)
    {
        float t = Mathf.InverseLerp(-80.0f, 0.0f, db);
        return Mathf.Pow(t, 4.0f);
    }
}
