﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Gameplay Event", menuName = "Evets/Basic Event")]
public class BasicGameplayEvent : ScriptableObject {

    public System.Action Event = delegate () { };

    void OnEnable()
    {
        Event += DebugText;
    }

    void OnDisable()
    {
        Event -= DebugText;
    }

    void DebugText()
    {
        Debug.LogFormat("Event {0} was called by {1}", name, StackTraceUtility.ExtractStackTrace());
    }
}
