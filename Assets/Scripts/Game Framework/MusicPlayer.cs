﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MusicPlayer : MonoBehaviour {

    #region Component
    [SerializeField]
    AudioSource m_Speaker;
    #endregion


    #region Static API

    static MusicPlayer m_Instance;
    [RuntimeInitializeOnLoadMethod]
    static void InstantiatePlayer()
    {
        m_Instance = Instantiate<MusicPlayer>(Resources.Load<MusicPlayer>("Music/MusicPlayer"));
        DontDestroyOnLoad(m_Instance.gameObject);
    }

    public static void SetMusic(AudioClip musicClip)
    {
        m_Instance.m_Speaker.clip = musicClip;
        m_Instance.m_Speaker.loop = true;
        m_Instance.m_Speaker.Play();
    }

    #endregion
}
