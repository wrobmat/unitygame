﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "TransformEvent", menuName = "Evets/Transform Event")]
public class TransformEvent : ScriptableObject {

    public System.Action<Transform> Event = delegate (Transform transform) { };
}
