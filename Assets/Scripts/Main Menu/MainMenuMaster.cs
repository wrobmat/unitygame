﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainMenuMaster : MonoBehaviour {

    public AudioClip BackgroundMusic;

	// Use this for initialization
	void Start () {
        MusicPlayer.SetMusic(BackgroundMusic);
	}
}
