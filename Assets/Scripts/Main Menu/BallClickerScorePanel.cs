﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BallClickerScorePanel : MonoBehaviour {
    const string HIghScoreFormat = "{0} - {1}: \t\t{2}";

    public List<Text> HighScoreEntries = new List<Text>();

    void Start()
    {
        PopulateHighScores();
    }

    void PopulateHighScores()
    {
        var scores = BallClickerHighScores.GetEntries();

        int savedScores = scores.Count;

        for (int i = 0; i < HighScoreEntries.Count; i++)
        {
            if (i < savedScores)
            {
                HighScoreEntries[i].text = string.Format(HIghScoreFormat, i + 1, scores[i].PlayerName, scores[i].Score);
            }
            else
            {
                HighScoreEntries[i].text = "";
            }
        }
    }

    public void ResetHighScores()
    {
        BallClickerHighScores.DeleteScores();
        PopulateHighScores();
    }
}