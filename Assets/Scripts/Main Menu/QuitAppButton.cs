﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QuitAppButton : MonoBehaviour {
    
    public void QuitApp()
    {
        Debug.Log("Quiting application");
        Application.Quit();
    }

}
