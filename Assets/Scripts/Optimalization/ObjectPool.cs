﻿using Assets.Scripts.Optimalization;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public static class ObjectPool {
    static Dictionary<PoolableObject, Stack<PoolableObject>> Pools = new Dictionary<PoolableObject, Stack<PoolableObject>>();

    public static bool AutoClearPoolsOnSceneChange = true;

    static ObjectPool()
    {
        SceneManager.activeSceneChanged += OnSceneChanged;
    }

    static void OnSceneChanged(Scene from, Scene to)
    {
        if (AutoClearPoolsOnSceneChange)
        {
            Pools.Clear();
        }
    }

    static public void PushIntoPool(PoolableObject poolable)
    {
        Stack<PoolableObject> stack = null;

        if (Pools.ContainsKey(poolable.SourcePrefab))
        {
            stack = Pools[poolable.SourcePrefab];
        }

        if (stack == null)
        {
            stack = new Stack<PoolableObject>();
            Pools.Add(poolable.SourcePrefab, stack);
        }

        stack.Push(poolable);
        poolable.OnHideInPool.Invoke();
    }

    static public PoolableObject GetFromPool(PoolableObject sourcePrefab)
    {
        PoolableObject pooledObject = null;

        if (Pools.ContainsKey(sourcePrefab))
        {
            var stack = Pools[sourcePrefab];
            if (stack.Count > 0)
            {
                pooledObject = stack.Pop();
            }

        }

        if (pooledObject == null)
        {
            pooledObject = Object.Instantiate<PoolableObject>(sourcePrefab);
        }

        pooledObject.SourcePrefab = sourcePrefab;
        pooledObject.OnBringFromPool.Invoke();

        return pooledObject;
    }
}
