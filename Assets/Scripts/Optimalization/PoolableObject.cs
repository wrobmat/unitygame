﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.Scripts.Optimalization
{
    public class PoolableObject : MonoBehaviour
    {
        [HideInInspector]
        public PoolableObject SourcePrefab;

        public Action OnHideInPool = delegate () { };
        public Action OnBringFromPool = delegate () { };

        public void Delete()
        {
            ObjectPool.PushIntoPool(this);
        }

        void Awake()
        {
            OnHideInPool += Hide;
            OnBringFromPool += Show;
        }

        private void Show()
        {
            gameObject.SetActive(true);
        }

        private void Hide()
        {
            gameObject.SetActive(false);
        }

        void Reset()
        {
#if UNITY_EDITOR
            var prefabType = 
                UnityEditor.PrefabUtility.GetPrefabType(gameObject);

            if (prefabType == UnityEditor.PrefabType.Prefab)
            {
                SourcePrefab = this;
            }

#endif
        }
    }
}
