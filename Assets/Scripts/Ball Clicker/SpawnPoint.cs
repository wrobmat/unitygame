﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnPoint : MonoBehaviour {

    public T Spawn<T>(T prefab, Transform spawnPosRot) where T : MonoBehaviour
    {
        T spawnedObject = Instantiate<T>(prefab, spawnPosRot.position, spawnPosRot.rotation);
        return spawnedObject;
    }

    /// <summary>
    /// Wywoływany tylko w edytorze
    /// </summary>
    void OnDrawGizmos()
    {
        Gizmos.color = Color.white;

        Gizmos.DrawWireCube(transform.position, Vector3.one);
    }
}
