﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BallClickerHUD : MonoBehaviour {

    [Header("Gameplay Events")]
    public BasicGameplayEvent OnAddPoints;
    public BasicGameplayEvent OnFail;

    const string PointFormat = "Points: {0}";
    const string EndScoreFormat = "Your score: {0}";

    [Header("UI Elements")]
    public GameObject GameOverPanel;
    public GameObject AddHighScorePanel;
    public InputField PlayerNameInputField;
    public Text Counter;
    public Text EndScore;
    int Points = 0;

    //[Header("Sound Settings")]

    void OnEnable()
    {
        Counter.text = string.Format(PointFormat, Points);
        GameOverPanel.SetActive(false);
        AddHighScorePanel.SetActive(false);
        OnAddPoints.Event += AddPoints;
        OnFail.Event += OnGameOver;
    }

    void OnDisable()
    {
        OnAddPoints.Event -= AddPoints;
        OnFail.Event -= OnGameOver;
    }

    void UpdatePointLabel () {
        Counter.text = string.Format(PointFormat, Points);
	}

    public void AddPoints()
    {
        Points++;
        UpdatePointLabel();
    }

    public void OnGameOver()
    {
        OnFail.Event -= OnGameOver;

        var entries = BallClickerHighScores.GetEntries();

        if (entries.Count < BallClickerHighScores.MaxEntries || entries[entries.Count - 1].Score < Points)
        {
            ShowAddHighScore();
        }
        else
        {
            DisableScorePanel_EnableGameOverPanel();
        }

        EndScore.text = string.Format(EndScoreFormat, Points);
    }

    private void DisableScorePanel_EnableGameOverPanel()
    {
        AddHighScorePanel.SetActive(false);
        GameOverPanel.SetActive(true);
    }

    public void SaveHighScore()
    {
        var entries = BallClickerHighScores.GetEntries();
        var entry = new HighScoreEntry()
        {
            PlayerName = (string.IsNullOrEmpty(PlayerNameInputField.text) ? "Anonim" : PlayerNameInputField.text),
            Score = Points,
        };
        entries.Add(entry);
        BallClickerHighScores.SavesEntries(entries);

        DisableScorePanel_EnableGameOverPanel();
    }

    private void ShowAddHighScore()
    {
        AddHighScorePanel.SetActive(true);
    }
}
