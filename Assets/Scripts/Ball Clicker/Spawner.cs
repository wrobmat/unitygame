﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour {

    private float SpawnEverySeconds = 1.0f;
    private WaitForSeconds waitTime;

    public BallToClick BallPrefab;
    int lastSpawnIndex = -1;
    public List<SpawnPoint> SpawnPoints = new List<SpawnPoint>();

    void OnEnable()
    {
        waitTime = new WaitForSeconds(SpawnEverySeconds);
        StartCoroutine(SpawnRoutine());
    }

    private IEnumerator SpawnRoutine()
    {
        while (true)
        {
            SpawnBall();
            yield return waitTime;
        }
    }

    private void SpawnBall()
    {
        lastSpawnIndex = (lastSpawnIndex + 1) % SpawnPoints.Count;

        SpawnPoint spawnPoint = SpawnPoints[lastSpawnIndex];
        var ball = ObjectPool.GetFromPool(BallPrefab.Poolable).GetComponent<BallToClick>();
        ball.ResetState();
        ball.transform.position = spawnPoint.transform.position;
        ball.transform.rotation = spawnPoint.transform.rotation;
        ball.gameObject.SetActive(true);
    }
}