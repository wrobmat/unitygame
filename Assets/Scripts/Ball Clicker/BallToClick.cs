﻿using Assets.Scripts.Optimalization;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallToClick : MonoBehaviour {

    const float LifeTime = 5.0f;
    const float MinSize = 0.0f;
    const float MaxSize = 4.0f;

    float m_LifeTime = 0.0f;

    [Header("Gameplay Event")]
    public BasicGameplayEvent OnClickEvent;
    public AudioClip SuccessSFX;
    public BasicGameplayEvent OnFailEvent;
    public AudioClip FailSFX;

    [Header("Event effects")]
    public ParticleSystem CLickedVFX;
    public ParticleSystem FailVFX;
    
    [Header("Component references")]
    public AudioSource AudioSpeaker;

    PoolableObject poolable;

    public PoolableObject Poolable
    {
        get
        {
            if (poolable == null)
            {
                poolable = GetComponent<PoolableObject>();
            }

            return poolable;
        }
    }

    void Awake()
    {
        ResetState();
    }

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        if (m_LifeTime > LifeTime)
        {
            NotifyFailed();
        }

        m_LifeTime += Time.deltaTime;

        UpdateSize();
	}

    private void DeleteObject()
    {
        Poolable.Delete();
    }

    public void NotifyClicked()
    {
        var poolableVFX = CLickedVFX.GetComponent<PoolableObject>();
        if (poolableVFX != null)
        {
            var vfx = ObjectPool.GetFromPool(poolableVFX);
            vfx.GetComponent<ParticleSystem>().Play();
            vfx.transform.position = transform.position;
        }

        Debug.Log("Ball is clicked");
        Poolable.Delete();

        OnClickEvent.Event.Invoke();
    }

    public void NotifyFailed()
    {
        var poolableVFX = FailVFX.GetComponent<PoolableObject>();
        if (poolableVFX != null)
        {
            var vfx = ObjectPool.GetFromPool(poolableVFX);
            vfx.GetComponent<ParticleSystem>().Play();
            vfx.transform.position = transform.position;
        }

        Debug.Log("You loose");
        DeleteObject();
        OnFailEvent.Event.Invoke();
    }

    void UpdateSize()
    {
        transform.localScale = Vector3.one * 
            Mathf.Lerp(MinSize, MaxSize, m_LifeTime / LifeTime);
    }

    public void ResetState()
    {
        m_LifeTime = 0.0f;
        UpdateSize();
    }
}
