﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

[Serializable]
public class BallClickerHighScores
{
    const string HighScoreJsonKey = "BallClicker_HighScores";
    public const int MaxEntries = 10;

    [SerializeField]
    List<HighScoreEntry> Scores = new List<HighScoreEntry>();

    public static List<HighScoreEntry> GetEntries()
    {
        string highScoreJSON = PlayerPrefs.GetString(HighScoreJsonKey, "");
        var scores = new BallClickerHighScores();
        JsonUtility.FromJsonOverwrite(highScoreJSON, scores);

        return scores.Scores;
    }

    public static void SavesEntries(List<HighScoreEntry> entries)
    {
        var scores = new List<HighScoreEntry>(entries);
        scores.Sort((score1, score2) => { return (-1) * (score1.Score - score2.Score); });

        if(scores.Count > MaxEntries)
        {
            scores.RemoveRange(MaxEntries - 1, scores.Count - MaxEntries);
        }

        var handler = new BallClickerHighScores();
        handler.Scores = scores;

        string scoresJSON = JsonUtility.ToJson(handler);
        PlayerPrefs.SetString(HighScoreJsonKey, scoresJSON);
    }

    public static void DeleteScores()
    {
        PlayerPrefs.DeleteKey(HighScoreJsonKey);
    }
}

[Serializable]
public class HighScoreEntry
{
    public string PlayerName = "";
    public int Score = 0;
}