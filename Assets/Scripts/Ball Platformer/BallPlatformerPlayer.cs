﻿using UnityEngine;

public class BallPlatformerPlayer : MonoBehaviour
{
    [Space]
    public float CheckDistanceMultiplier = 1.1f;
    public float CheckRadiusMultiplier = 0.75f;

    float CurrentHorizontalInput = 0.0f;

    public float MaxSelfPropelledTorque = 5.0f;
    public float Acceleration = 5.0f;
    public float JumpForce = 250.0f;

    Rigidbody m_RB;
    Rigidbody RB
    {
        get
        {
            if (m_RB == null)
            {
                m_RB = GetComponent<Rigidbody>();
            }

            return m_RB;
        }
    }

    SphereCollider m_SphereCollider;
    SphereCollider SphereCollider
    {
        get
        {
            if (m_SphereCollider == null)
            {
                m_SphereCollider = GetComponent<SphereCollider>();
            }

            return m_SphereCollider;
        }
    }

    void Update()
    {
        CurrentHorizontalInput = 0.0f;

        if (Input.GetKey(KeyCode.LeftArrow) || Input.GetKey(KeyCode.A))
        {
            CurrentHorizontalInput += 1.0f;
        }

        if (Input.GetKey(KeyCode.RightArrow) || Input.GetKey(KeyCode.D))
        {
            CurrentHorizontalInput -= 1.0f;
        }

        RaycastHit hitInfo;
        if (Physics.SphereCast(transform.position, SphereCollider.radius * CheckRadiusMultiplier, Vector3.down, out hitInfo, SphereCollider.radius * CheckDistanceMultiplier))
        {
            if (Input.GetKeyDown(KeyCode.Space) || Input.GetKey(KeyCode.W))
            {
                RB.AddForce(Vector3.up * JumpForce);
            }
        }
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        float currentTorqueMagnitude = RB.angularVelocity.z / MaxSelfPropelledTorque;
        float accelerationMultiplier = 1.0f;

        if (Mathf.Sign(CurrentHorizontalInput) == Mathf.Sign(currentTorqueMagnitude))
        {
            accelerationMultiplier = 1.0f - Mathf.Abs(currentTorqueMagnitude);
        }

        RB.AddTorque(Vector3.forward * CurrentHorizontalInput * accelerationMultiplier * Acceleration);
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.yellow;
        Gizmos.DrawSphere(transform.position + Vector3.down * SphereCollider.radius * CheckDistanceMultiplier, SphereCollider.radius * CheckRadiusMultiplier);
    }
}
