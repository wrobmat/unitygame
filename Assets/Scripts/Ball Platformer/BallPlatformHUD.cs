﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BallPlatformHUD : MonoBehaviour {

    const string InGameLiveCountTextFormat = "LIVES: {0}";
    public BasicGameplayEvent OnGameOverEvent;
    public SharedInt LiveCount;

    [Header("UI Elements")]
    public Text LiveCountLabel;
    public GameObject GameOverPanel;

    private void OnEnable()
    {
        UpdateLiveCountLabel(LiveCount.Value);
        LiveCount.OnChanged += UpdateLiveCountLabel;
        GameOverPanel.SetActive(false);
        OnGameOverEvent.Event += OnGameOver;
    }

    private void OnDisable()
    {
        LiveCount.OnChanged -= UpdateLiveCountLabel;
        OnGameOverEvent.Event -= OnGameOver;
    }

    void UpdateLiveCountLabel(int value)
    {
        LiveCountLabel.text = string.Format(InGameLiveCountTextFormat, value);
    }

    void OnGameOver()
    {
        GameOverPanel.SetActive(true);
    }
}
