﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheckpointTrigger : MonoBehaviour {

    public TransformEvent OnPlayerEntered;

    private void OnTriggerEnter(Collider other)
    {
        if (other.attachedRigidbody == null)
        {
            return;
        }

        if (other.attachedRigidbody.tag == "Player")
        {
            Debug.Log("Player in Checkpoint");
            OnPlayerEntered.Event.Invoke(transform);
        }
    }

    private void OnValidate()
    {
        if (OnPlayerEntered == null)
        {
            Debug.LogWarning("TransformEvent OnPlayerEntered cannot be null");
        }
    }
}
