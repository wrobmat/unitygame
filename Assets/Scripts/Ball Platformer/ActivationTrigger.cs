﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActivationTrigger : MonoBehaviour {
    public GameObject ToActivate;

    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            ToActivate.SetActive(true);
        }
    }
}
