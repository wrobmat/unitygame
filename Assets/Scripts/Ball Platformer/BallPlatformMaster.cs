﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallPlatformMaster : MonoBehaviour {

    public int InitialLiveCount = 5;
    public SharedInt Lives;
    public BasicGameplayEvent DamageTakenEvent;
    public BasicGameplayEvent GameOverEvent;
    public TransformEvent UpdateCheckpointEvent;
    public Transform StartPosition;
    public BallPlatformerPlayer Player; //in more ambitious game we would like to spawn it from prefab. For small games like this it's ok to have one on scene already.

    Vector3 CheckPointPosition;

    private void Awake()
    {
        Lives.Value = InitialLiveCount;
        CheckPointPosition = StartPosition.position;
    }

    private void Start()
    {
        TeleportPlayerToCheckpoint();
    }

    private void OnEnable()
    {
        DamageTakenEvent.Event += OnPlayerTakenDamage;
        UpdateCheckpointEvent.Event += UpdateCheckpoint;

    }

    private void OnDisable()
    {
        DamageTakenEvent.Event -= OnPlayerTakenDamage;
        UpdateCheckpointEvent.Event -= UpdateCheckpoint;
    }

    private void OnPlayerTakenDamage()
    {
        Lives.Value--;

        if (Lives.Value <= 0)
        {
            OnGameOver();
        }
        else
        {
            TeleportPlayerToCheckpoint();
        }
    }

    private void OnGameOver()
    {
        Debug.Log("GAME OVER");
        Destroy(Player.gameObject);
        GameOverEvent.Event.Invoke();
    }

    private void UpdateCheckpoint(Transform checkpoint)
    {
        CheckPointPosition = checkpoint.position;
    }

    private void OnValidate()
    {
        if (Lives == null)
        {
            Debug.LogWarning("We need to setup SharedInt Lives");
        }

        if (DamageTakenEvent == null)
        {
            Debug.LogWarning("We need to setup BasicGameplayEvent OnDamageTaken");
        }

        if(DamageTakenEvent == null)
        {
            Debug.LogWarning("We need to setup Transform StartPosition");
        }

        if (UpdateCheckpointEvent == null)
        {
            Debug.LogWarning("We need to setup TransformEvent UpdateCheckpointEvent");
        }

        if (GameOverEvent == null)
        {
            Debug.LogWarning("We need to setup BasicGameplayEvent GameOverEvent");
        }
    }

    private void TeleportPlayerToCheckpoint()
    {
        Player.transform.position = CheckPointPosition;
        Player.transform.rotation = Quaternion.identity;
        var playerRigidbody = Player.GetComponent<Rigidbody>();
        playerRigidbody.velocity = Vector3.zero;
        playerRigidbody.angularVelocity = Vector3.zero;
    }
}
