﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DamageTrigger : MonoBehaviour {

    public BasicGameplayEvent OnPlayerHit;

    private void OnTriggerEnter(Collider other)
    {
        if (other.attachedRigidbody == null)
        {
            return;
        }

        if (other.attachedRigidbody.tag == "Player")
        {
            OnPlayerHit.Event.Invoke();
        }
    }

    private void OnValidate()
    {
        if (OnPlayerHit == null)
        {
            Debug.LogWarning("BasicGameplayEvent OnPlayerHit cannot be null");
        }
    }
}
